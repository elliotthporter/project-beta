# CarCar

CarCar is a state-of-the-art application with three microservices, Sales, Inventory, and Services. The app is designed to help consolidate and organize a car dealership's records of automobiles, customers, technicians, service appointments, service history and more.

Team:

* Elliott - Services and Inventory

In order to run the application you should first install Docker, Git, and Node.js 18.2 or later.

Then:

Fork and clone the repo located at https://gitlab.com/elliotthporter/project-beta

Open your Docker Desktop Application and run the following commands while in the correct project directory.

    docker volume create beta-data
    docker-compose build
    docker-compose up

This should create and run the proper containers needed to run CarCar.

The main port for users to interact with the project on their browser is:

http://localhost:3000/

The services microservice exists on:

http://localhost:8080/

Sales:

http://localhost:8090/

Inventory:

http://localhost:8100/

![Img](/images/Screenshot%202024-03-22%20at%201.49.37%20PM.png)

The Services Microservice:

The Technician model enables dealerships to effectively manage their technician workforce. Each technician is identified by their first name, last name, and employee ID, providing a comprehensive overview of staff members.

Regarding integration with the inventory microservice, our models ensure seamless coordination between dealership operations and inventory management systems.

Moving on to the Appointment model, it facilitates the scheduling of service appointments. The Appointment Form offers fields to input crucial details such as the vehicle's VIN, customer name, appointment date and time, preferred technician selection from available staff, and a brief description of the appointment reason.

Notably, if the vehicle's VIN corresponds to an inventory item, this information is used to determine VIP status for customers during appointment viewing. Additionally, users have the capability to mark appointments as canceled or completed directly from the appointment list interface.

Leveraging data from the Appointment model, users can access a comprehensive Service History. This feature allows for easy retrieval of vehicle service records by VIN, providing a detailed summary of each appointment along with the current appointment status.

The Service microservice employs a singular Automobile value object, which grants access to automobile models stored in the Inventory Microservice. The AutomobileVO initially sets the sold field value to false, enabling future modifications on the Sales microservice end. Additionally, it includes a VIN field to uniquely identify vehicles.

----


Service API Endpoints and Documentation:

![Img](/images/Screenshot%202024-03-22%20at%201.29.01%20PM.png)

GET Technicians Returns the body:

	{
		"id": 1,
		"first_name": "Spiro",
		"last_name": "Agnew",
		"employee_id": "2"
	},

---------------

POST Technician JSON Body example:

{
  "first_name": "gerald",
  "last_name": "donald",
  "employee_id": "5"
}

Returns:

{
	"message": "Technician created successfully",
	"technician_id": 22
}

--------------

DELETE Technician Returns (with appropriate ID in endpoint)

{
    "message": "Technician deleted successfully",
}

---------------

GET Appointments Returns:

[
	{
		"id": 2,
		"date_time": "2024-02-01T12:30:00+00:00",
		"reason": "Routine inspection",
		"status": "Scheduled",
		"vin": "15214",
		"customer": "Ronald McDonald",
		"technician_id": 1,
		"vip": false
	},

---------------

CREATE Appointmnent JSON Body Example:

{
  "date_time": "2024-02-17T12:30:00",
  "reason": "Car broken",
  "status": "Scheduled",
  "vin": "15214",
  "customer": "Crazy Jim",
  "technician": 1
}

Returns:

{
	"message": "Appointment created successfully",
	"appointment_id": 22
}

---------------

DELETE Appointment Returns with the appropriate ID in Endpoint:

{
	"message": "Appointment deleted successfully"
}

---------------

USE A PUT request with /:id/cancel to set appointment to cancelled
USE A PUT request with /:id/finish to set appointment to finished

----

Inventory Microservice:

First off, you can easily check out all the different manufacturers whose cars you've got in stock. Want to add a new manufacturer to the mix? No problem! Just hop on over to the microservice, and you can create a new entry for them.

Then there are the vehicle models. This microservice lays out all the models you've got available, so you can see what's in your inventory at a glance. And hey, if you get a new model in, just add it in there too!

Of course, you'll want to see all the actual cars you've got sitting on the lot, right? Well, the Inventory microservice lets you do just that. It's got a nifty list showing every single car, complete with details like the model, manufacturer, and whatever else you need to know.

And guess what? You can add new cars to the inventory too! Just punch in the details, like which model it is and who makes it, and boom, it's in there.

So yeah, that's the lowdown on the Inventory microservice. It's like having your own personal assistant for managing your car stock, making your dealership life a whole lot easier!

HOW FUN!!!

Inventory API endpoints and examples:

![Img](/images/Screenshot%202024-03-22%20at%202.13.57%20PM.png)
![Img](/images/Screenshot%202024-03-22%20at%202.14.05%20PM.png)
![Img](/images/Screenshot%202024-03-22%20at%202.14.11%20PM.png)

GET Models List Returns:

{
	"models": [
		{
			"href": "/api/models/1/",
			"id": 1,
			"name": "Sebring",
			"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
			"manufacturer": {
				"href": "/api/manufacturers/1/",
				"id": 1,
				"name": "Chrysler"
			}
		},

---------------

POST Vehicle Model JSON Body example

{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}

Returns:

{
	"href": "/api/models/8/",
	"id": 8,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Chrysler"
	}
}



Use a GET request with the appropriate ID number in the endpoint to get a certain model
Use a PUT request with the appropriate ID number in the endpoint to update a certain model
Use a DELETE request with the appropriate ID number in the endpoint to delete a model

---------------

GET Specific Model with ID in Endpoint Returns:

{
	"href": "/api/models/1/",
	"id": 1,
	"name": "Sebring",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/1/",
		"id": 1,
		"name": "Chrysler"
	}
}

---------------

GET Automobiles List returns

{
	"autos": [
		{
			"href": "/api/automobiles/1C3CC5FB2AN120174/",
			"id": 1,
			"color": "red",
			"year": 2012,
			"vin": "1C3CC5FB2AN120174",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Sebring",
				"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Chrysler"
				}
			},
			"sold": false
		},

---------------

POST Automobile JSON body:

{
  "color": "Teal",
  "year": 2014,
  "vin": "351351351ASGA",
  "model_id": 2
}

Returns:

{
	"href": "/api/automobiles/351351351ASGA/",
	"id": 3,
	"color": "Teal",
	"year": 2014,
	"vin": "351351351ASGA",
	"model": {
		"href": "/api/models/2/",
		"id": 2,
		"name": "PT Cruiser",
		"picture_url": "https://www.motortrend.com/uploads/sites/11/2020/04/2003_PT_Cruiser.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Toyota"
		}
	},
	"sold": false
}

---------------

GET Specific Automobile by Vin at end of API endpoint returns:

{
	"href": "/api/automobiles/1C3CC5FB2AN120174/",
	"id": 1,
	"color": "red",
	"year": 2012,
	"vin": "1C3CC5FB2AN120174",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Sebring",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	},
	"sold": false
}

Use PUT request to update Specific Automobile with Vin at end of API endpoint
Use DELETE request to update Specific Automobile with Vin at end of API endpoint

----

GET Manufacturers List returns:

{
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Chrysler"
		}
	]
}

---------------

POST Manufacturer example JSON body:

{
  "name": "Chrysler"
}


returns:

{
	"href": "/api/manufacturers/1/",
	"id": 1,
	"name": "Chrysler"
}

Use a GET request with the appropriate ID number in the endpoint to get a certain Manufacturer
Use a PUT request with the appropriate ID number in the endpoint to update a certain Manufacturer
Use a DELETE request with the appropriate ID number in the endpoint to delete a Manufacturer
