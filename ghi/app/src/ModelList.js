import React, { useState, useEffect } from 'react';

const ModelList = () => {
  const [models, setModels] = useState([]);

  useEffect(() => {
    fetch('http://localhost:8100/api/models/')
      .then(response => response.json())
      .then(data => setModels(data.models))
      .catch(error => console.error(error));
  }, []);

  return (
    <div className="container mt-5">
      <h2 className="text-center mb-4">Vehicle Models</h2>
      <div className="row row-cols-1 row-cols-md-3 g-4">
        {models.map(model => (
          <div key={model.id} className="col mb-4">
            <div className="card h-100">
              <img src={model.picture_url} className="card-img-top" alt={model.name} />
              <div className="card-body">
                <h5 className="card-title">{model.name}</h5>
                <p className="card-text">Manufacturer: {model.manufacturer.name}</p>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default ModelList;
