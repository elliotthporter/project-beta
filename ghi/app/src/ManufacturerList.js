import { useEffect, useState } from 'react';

function ManufacturerList() {
    const [manufacturers, setManufacturers] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/');

        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    useEffect(()=>{
        getData();
    }, []);

    return (
        <div className="container mt-5">
            <h1 className="text-center mb-4">Manufacturers</h1>
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <table className="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th className="text-center">Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            {manufacturers.map((manufacturer, index) => (
                                <tr key={index}>
                                    <td className="text-center">{manufacturer.name}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
}

export default ManufacturerList;
