import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ModelList from './ModelList';
import AddVehicleModelForm from './AddVehicleModelForm';
import ManufacturerList from './ManufacturerList';
import CreateManufacturerForm from './CreateManufacturerForm';
import AutomobileList from './AutomobileList';
import AddAutomobileForm from './AddAutomobileForm';
import TechnicianList from './TechnicianList';
import Addtechnician from './Addtechnician';
import Serviceappointmentform from './Serviceappointmentform';
import Serviceappointmentlist from './Serviceappointmentlist';
import ServiceHistory from './ServiceHistory';


const rootStyle = {
  fontFamily: 'Arial, sans-serif',
  minHeight: '100vh',
  background: '#c7e6ff',
  color: '#363636',
  padding: '20px',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
};

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div style={rootStyle}>
      <div className="container">
        <Routes>
          <Route path="/models/list" element={<ModelList />} />
          <Route path="/" element={<MainPage />} />
          <Route path="/models/create" element={<AddVehicleModelForm />} />
          <Route path="/manufacturers/list" element={<ManufacturerList />} />
          <Route path="/manufacturer/create" element={<CreateManufacturerForm />} />
          <Route path="/automobiles/list" element={<AutomobileList />} />
          <Route path="/create/automobile" element={<AddAutomobileForm />} />
          <Route path="/technicians/list" element={<TechnicianList />} />
          <Route path="/create/technicians" element={<Addtechnician />} />
          <Route path="/create/appointment" element={<Serviceappointmentform />} />
          <Route path="/appointment/list" element={<Serviceappointmentlist />} />
          <Route path="/service/history" element={<ServiceHistory />} />
        </Routes>
      </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
