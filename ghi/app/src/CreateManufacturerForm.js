import React, { useState } from 'react';

const AddManufacturerForm = () => {
  const [manufacturerName, setManufacturerName] = useState('');

  const handleChange = (e) => {
    setManufacturerName(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    fetch('http://localhost:8100/api/manufacturers/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ name: manufacturerName }),
    })
      .then(response => response.json())
      .then(data => {
        console.log(data);
        alert('Manufacturer added successfully!');
        setManufacturerName('');
      })
      .catch(error => {
        console.error(error);
        alert('Failed to add manufacturer. Please try again.');
      });
  };

  return (
    <div className="container d-flex justify-content-center align-items-center" style={{ minHeight: '80vh' }}>
      <div className="col-md-6 border p-4 rounded shadow-lg">
        <h1 className="mb-4 text-center">Create a New Manufacturer</h1>
        <form onSubmit={handleSubmit}>
          <div className="mb-3">
            <label htmlFor="manufacturerName" className="form-label">Manufacturer Name:</label>
            <input
              type="text"
              className="form-control"
              id="manufacturerName"
              name="manufacturerName"
              value={manufacturerName}
              onChange={handleChange}
              required
              placeholder="Enter manufacturer name"
            />
          </div>

          <div className="text-center">
            <button type="submit" className="btn btn-success">Create Manufacturer</button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default AddManufacturerForm;
