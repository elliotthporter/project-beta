import React, { useState, useEffect } from 'react';

const ServiceAppointmentForm = () => {
    const [formData, setFormData] = useState({
        date_time: '',
        reason: '',
        vin: '',
        customer: '',
        technician: ''
    });
    const [technicians, setTechnicians] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8080/api/technicians/')
            .then(response => response.json())
            .then(data => {
                setTechnicians(data);
            })
            .catch(error => {
                console.error('Error fetching technicians:', error);
            });
    }, []);

    const handleChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        fetch('http://localhost:8080/api/appointments/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                ...formData,
                status: 'Scheduled'
            }),
        })
        .then(response => response.json())
        .then(data => {
            console.log('Success:', data);
            alert('Appointment created successfully');
        })
        .catch(error => {
            console.error('Error:', error);
            alert('Failed to create appointment');
        });
    };

    return (
        <form onSubmit={handleSubmit}>
            <div className="mb-3">
                <label htmlFor="date_time" className="form-label">Date and Time:</label>
                <input type="datetime-local" id="date_time" name="date_time" value={formData.date_time} onChange={handleChange} className="form-control" required/>
            </div>

            <div className="mb-3">
                <label htmlFor="reason" className="form-label">Reason:</label>
                <input type="text" id="reason" name="reason" value={formData.reason} onChange={handleChange} className="form-control" required/>
            </div>

            <div className="mb-3">
                <label htmlFor="vin" className="form-label">VIN:</label>
                <input type="text" id="vin" name="vin" value={formData.vin} onChange={handleChange} className="form-control" required/>
            </div>

            <div className="mb-3">
                <label htmlFor="customer" className="form-label">Customer:</label>
                <input type="text" id="customer" name="customer" value={formData.customer} onChange={handleChange} className="form-control" required/>
            </div>

            <div className="mb-3">
                <label htmlFor="technician" className="form-label">Technician:</label>
                <select id="technician" name="technician" value={formData.technician} onChange={handleChange} className="form-select" required>
                    <option value="">Select Technician</option>
                    {technicians.map(technician => (
                        <option key={technician.id} value={technician.id}>
                            {technician.first_name} {technician.last_name}
                        </option>
                    ))}
                </select>
            </div>

            <button type="submit" className="btn btn-primary">Submit</button>
        </form>
    );
};

export default ServiceAppointmentForm;
