import React, { useState, useEffect } from 'react';

const ServiceHistory = () => {
    const [appointments, setAppointments] = useState([]);
    const [filteredAppointments, setFilteredAppointments] = useState([]);
    const [searchVin, setSearchVin] = useState('');

    useEffect(() => {
        fetch('http://localhost:8080/api/appointments/')
            .then(response => response.json())
            .then(data => {
                setAppointments(data);
            })
            .catch(error => {
                console.error('Error fetching appointments:', error);
            });
    }, []);

    const handleSearch = () => {
        const filtered = appointments.filter(appointment => appointment.vin.includes(searchVin));
        setFilteredAppointments(filtered);
    };

    return (
        <div className="container mt-5">
            <h2 className="mb-4">Service History</h2>
            <div className="input-group mb-3">
                <input
                    type="text"
                    className="form-control"
                    placeholder="Enter VIN"
                    value={searchVin}
                    onChange={e => setSearchVin(e.target.value)}
                />
                <button className="btn btn-primary" type="button" onClick={handleSearch}>Search</button>
            </div>
            <ul className="list-group">
                {filteredAppointments.map(appointment => (
                    <li key={appointment.id} className="list-group-item">
                        <strong>VIN:</strong> {appointment.vin}<br/>
                        <strong>Customer:</strong> {appointment.customer}<br/>
                        <strong>Date and Time:</strong> {new Date(appointment.date_time).toLocaleString()}<br/>
                        <strong>Technician:</strong> {appointment.technician ? `${appointment.technician.first_name} ${appointment.technician.last_name}` : 'N/A'}<br/>
                        <strong>Reason:</strong> {appointment.reason}<br/>
                        <strong>Status:</strong> {appointment.status}<br/>
                        {appointment.vip && <span>VIP</span>}<br/>
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default ServiceHistory;
