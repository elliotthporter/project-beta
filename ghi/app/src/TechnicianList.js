import React, { useEffect, useState } from "react";

function TechnicianList() {
    const [technicians, setTechnicians] = useState([]);

    const getTechnicianData = async () => {
        try {
            const technicianResponse = await fetch('http://localhost:8080/api/technicians/');

            if (!technicianResponse.ok) {
                throw new Error('Failed to fetch technicians data');
            }

            const technicianData = await technicianResponse.json();
            setTechnicians(technicianData);
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        getTechnicianData();
    }, []);

    return (
        <div className="container mt-5">
            <h2 className="text-center mb-4">Technician List</h2>
            <table className="table table-hover align-middle">
                <thead>
                    <tr>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Employee ID</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians.map(technician => (
                        <tr key={technician.id}>
                            <td>{ technician.first_name }</td>
                            <td>{ technician.last_name }</td>
                            <td>{ technician.employee_id }</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default TechnicianList;
