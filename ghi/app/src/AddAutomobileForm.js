import React, { useState } from 'react';

const AddAutomobileForm = () => {
  const [formData, setFormData] = useState({
    color: '',
    year: '',
    vin: '',
    model_id: '',
  });

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    const postData = {
      color: formData.color,
      year: parseInt(formData.year),
      vin: formData.vin,
      model_id: parseInt(formData.model_id),
    };

    fetch('http://localhost:8100/api/automobiles/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(postData),
    })
    .then(response => {
      if (response.ok) {
        return response.json();
      }
      throw new Error('Failed to create automobile');
    })
    .then(data => {
      console.log('Success:', data);
      window.alert('Automobile submitted successfully!');
    })
    .catch((error) => {
      console.error('Error:', error);
      window.alert('Failed to submit automobile. Please try again.');
    });
  };

  return (
    <div className="container mt-5">
      <h2 className="text-center mb-4">Add New Automobile</h2>
      <div className="row justify-content-center">
        <div className="col-md-6">
          <form onSubmit={handleSubmit}>
            <div className="mb-3">
              <label htmlFor="color" className="form-label">Color:</label>
              <input type="text" className="form-control" id="color" name="color" value={formData.color} onChange={handleChange} required />
            </div>
            <div className="mb-3">
              <label htmlFor="year" className="form-label">Year:</label>
              <input type="number" className="form-control" id="year" name="year" value={formData.year} onChange={handleChange} required />
            </div>
            <div className="mb-3">
              <label htmlFor="vin" className="form-label">VIN:</label>
              <input type="text" className="form-control" id="vin" name="vin" value={formData.vin} onChange={handleChange} required />
            </div>
            <div className="mb-3">
              <label htmlFor="model_id" className="form-label">Model ID:</label>
              <input type="number" className="form-control" id="model_id" name="model_id" value={formData.model_id} onChange={handleChange} required />
            </div>
            <div className="text-center">
              <button type="submit" className="btn btn-primary">Add Automobile</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default AddAutomobileForm;
