import React, { useState, useEffect } from 'react';

const ServiceAppointmentList = () => {
    const [appointments, setAppointments] = useState([]);
    const [appointmentVipStatus, setAppointmentVipStatus] = useState({});

    useEffect(() => {
        fetchAppointments();
    }, []);

    const fetchAppointments = () => {
        fetch('http://localhost:8080/api/appointments/')
            .then(response => response.json())
            .then(data => {
                setAppointments(data.filter(app => app.status !== 'Canceled' && app.status !== 'Finished'));
                Promise.all(data.map(appointment => checkIfVip(appointment.vin)))
                    .then(vipStatuses => {
                        const vipStatusMap = {};
                        data.forEach((appointment, index) => {
                            vipStatusMap[appointment.id] = vipStatuses[index];
                        });
                        setAppointmentVipStatus(vipStatusMap);
                    })
                    .catch(error => {
                        console.error('Error checking VIP status:', error);
                    });
            })
            .catch(error => {
                console.error('Error fetching appointments:', error);
            });
    };

    const checkIfVip = async (vin) => {
        try {
            const response = await fetch(`http://localhost:8100/api/automobiles/`);
            const data = await response.json();
            return data.autos.some(auto => auto.vin === vin);
        } catch (error) {
            console.error('Error checking VIP status:', error);
            return false;
        }
    };

    const handleCancelAppointment = (id) => {
        fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ status: 'Canceled' }),
        })
        .then(response => response.json())
        .then(data => {
            setAppointments(prevAppointments => prevAppointments.filter(appointment => appointment.id !== id));
            console.log('Appointment canceled:', data);
        })
        .catch(error => {
            console.error('Error canceling appointment:', error);
        });
    };

    const handleFinishAppointment = (id) => {
        fetch(`http://localhost:8080/api/appointments/${id}/finish/`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ status: 'Finished' }),
        })
        .then(response => response.json())
        .then(data => {
            setAppointments(prevAppointments => prevAppointments.filter(appointment => appointment.id !== id));
            console.log('Appointment finished:', data);
        })
        .catch(error => {
            console.error('Error finishing appointment:', error);
        });
    };

    return (
        <div className="container mt-4">
            <h2 className="mb-3">Service Appointments</h2>
            <div className="table-responsive">
                <table className="table table-hover shadow-sm">
                    <thead className="table-light">
                        <tr>
                            <th>VIN</th>
                            <th>Customer</th>
                            <th>Date and Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th>VIP</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {appointments.map(appointment => (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{appointment.customer}</td>
                                <td>{new Date(appointment.date_time).toLocaleString()}</td>
                                <td>{appointment.technician ? `${appointment.technician.first_name} ${appointment.technician.last_name}` : 'N/A'}</td>
                                <td>{appointment.reason}</td>
                                <td>{appointmentVipStatus[appointment.id] ? <span className="badge bg-success">VIP</span> : 'No'}</td>
                                <td>
                                    {appointment.status !== 'Canceled' && appointment.status !== 'Finished' && (
                                        <>
                                            <button className="btn btn-danger me-2" onClick={() => handleCancelAppointment(appointment.id)}>Cancel</button>
                                            <button className="btn btn-success" onClick={() => handleFinishAppointment(appointment.id)}>Finish</button>
                                        </>
                                    )}
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
};

export default ServiceAppointmentList;
